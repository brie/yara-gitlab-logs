rule redisNowReady
{
    meta:
        author                = "Brie Carranza"
        description           = "Check the Redis version and whether it is up and ready to accept connections"
        troubleshooting       = "https://docs.gitlab.com/ee/administration/redis/troubleshooting.html"

    strings:
        $redis_version_short = /Redis version=\d{1,2}\.\d{1,2}\.\d{1,2}/
        $redis_version_commit = /Redis \d{1,2}\.\d{1,2}\.\d{1,2}\s\(.{1,10}\).{0,35}/
        $rdb_produced_by_version = /Loading\sRDB.{21}\d{1,2}\.\d{1,2}\.\d{1,2}/ 
        $ready_to_accept = "The server is now ready to accept connections"

    condition:
        any of them
}
