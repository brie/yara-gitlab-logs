rule sidekiqClasses     : SIDEKIQ HEALTH
{
    meta:
        author                = "Brie Carranza"
        about                 = "GitLab stores Sidekiq jobs and their arguments in Redis."
        configuration         = "https://docs.gitlab.com/ee/administration/sidekiq/index.html"
        description           = "Check the Redis version and whether it is up and ready to accept connections"
        rule_source           = "https://gitlab.com/brie/yara-gitlab-logs/-/blob/main/rules/sidekiq/classes.yara"
        troubleshooting       = "https://docs.gitlab.com/ee/administration/sidekiq/sidekiq_troubleshooting.html"


    strings:
        $sidekiq_workers = /"{0,1}[a-zA-Z]{0,2}\:{0,2}[A-Z]{1}[a-zA-Z]{0,99}Worker"/

    condition:
        any of them
}
