rule gitalyListening   : OK
{
    meta:
        author        = "Brie Carranza"
        description   = "Gitaly is up and ready to accept connections https://docs.gitlab.com/ee/administration/gitaly/troubleshooting.html"

    strings:
        $listening_at_address = "listening at address"

    condition:
        $listening_at_address
}
