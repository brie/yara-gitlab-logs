rule gitalyRepoCounter   : CONFIG GITALY
{
    meta:
        author        = "Brie Carranza"
        description   = "Observe as Gitaly counts repositories https://docs.gitlab.com/ee/administration/gitaly/configure_gitaly.html"
        src_code      = "https://gitlab.com/gitlab-org/gitaly/-/blob/613fa6169f5750468bdd4a15b1f453ea4f17c45c/internal/gitaly/storage/counter/counter.go"

    strings:
        $start_count_text = "starting to count"
        $complete_count_text = "completed counting"
        $complete_count_all_text = "completed counting all repositories"
        $start_count_regex = /start.{0,99}count.{0,99}storage_path.{0,99}/
        $complete_count_regex = /completed.{0,99}count.{0,99}storage_path.{0,99},/
        $complete_count_all_regex = /completed.{0,99}counting.{0,99}all.{0,99},/


    condition:
        any of them
}
