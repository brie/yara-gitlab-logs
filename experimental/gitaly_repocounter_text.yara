rule gitalyRepoCounterText   : CONFIG GITALY
{
    meta:
        author        = "Brie Carranza"
        description   = "Observe as Gitaly counts repositories https://docs.gitlab.com/ee/administration/gitaly/configure_gitaly.html"
        src_code      = "https://gitlab.com/gitlab-org/gitaly/-/blob/613fa6169f5750468bdd4a15b1f453ea4f17c45c/internal/gitaly/storage/counter/counter.go"

    strings:
        $start_count_text = "starting to count"
        $complete_count_text = "completed counting"
        $complete_count_all_text = "completed counting all repositories"

    condition:
        any of them
}
