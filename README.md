# 🧪 Experiment: using YARA rules to analyze GitLab logs

I published these rules to accompany a blog post I wrote on [brie.dev](https://brie.dev). You are welcome to let these rules inspire your efforts. 

## ✨ Miscellaneous

A [regular expression](https://regex101.com/r/EWhF3M/1) for capturing when `gitaly` starts and stops counting repositories:

```
/(start|complet).{0,}count[a-z]{0,3} repositories.{0,}storage_path.{0,}/gm
```
